package de.ur.iw.optapeb.ruleseditor.ui;

import static com.google.common.base.Charsets.UTF_8;
import static de.ur.iw.optapeb.ruleseditor.FileHelper.DEFAULT_FILENAME_FOR_RULES;
import static de.ur.iw.optapeb.ruleseditor.FileHelper.getCurrentWorkingDirectory;
import static de.ur.iw.optapeb.ruleseditor.FileHelper.loadEmbeddedModelAndNoRules;
import static de.ur.iw.optapeb.ruleseditor.FileHelper.loadModelAndRules;
import static de.ur.iw.optapeb.ruleseditor.ui.RulesEditor.PREFERENCES_KEY_LAST_RULES_FILE;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import com.google.common.io.Resources;

import de.ur.iw.optapeb.ruleseditor.model.ModelAndRules;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainClass extends Application {
	/**
	 * @return the <code>Preferences</code> instance representing this program.
	 */
	public static Preferences preferences() {
		return Preferences.userNodeForPackage(MainClass.class);
	}

	public static final String PREFERENCES_KEY_RULE_TESTER_VISIBLE = "ruleTesterVisible";

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// try to load model and rules from current working directory, else use default
		// model and no rules
		boolean firstUsage = MainClass.preferences().keys().length == 0;
		if (firstUsage) {
			String introduction = Resources.asCharSource(Resources.getResource("help/introduction.html"), UTF_8).read();
			new RichTextPopup("Willkommen", introduction, MainClass::openRulesEditorAndTester);
		} else {
			openRulesEditorAndTester();
		}
	}

	private static Void openRulesEditorAndTester() throws IOException {
		// load last opened model and rules - or start with empty model and rules
		String rulesFile = MainClass.preferences().get(PREFERENCES_KEY_LAST_RULES_FILE, DEFAULT_FILENAME_FOR_RULES);
		File pathToRules = getCurrentWorkingDirectory().resolve(rulesFile).toFile();
		ModelAndRules modelAndRules = pathToRules.exists() ? loadModelAndRules(pathToRules)
				: loadEmbeddedModelAndNoRules();

		// open the actually useful windows
		RulesEditor rulesEditor = new RulesEditor(modelAndRules);
		Decider ruleTester = new Decider(modelAndRules.model, modelAndRules.rules);

		// enable interaction between them
		rulesEditor.setRuleTester(ruleTester);
		ruleTester.setRulesEditor(rulesEditor);
		
		return null;
	}

}
