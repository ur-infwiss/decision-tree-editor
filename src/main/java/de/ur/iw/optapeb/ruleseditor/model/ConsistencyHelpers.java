package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

import java.util.HashSet;
import java.util.List;

public class ConsistencyHelpers {
	public static void checkNoDuplicateElements(List<?> elements) {
		checkArgument(new HashSet<>(elements).size() == elements.size(),
				reflectionToString(elements, JSON_STYLE) + " must not contain duplicate elements!");
	}

}
