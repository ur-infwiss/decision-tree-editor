package de.ur.iw.optapeb.ruleseditor.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains one possible set of values of a patient's modelled properties.
 */
public class PatientState {
	public final Map<Property, String> propertyValues = new HashMap<>();
}
