package de.ur.iw.optapeb.ruleseditor.ui;

import java.util.Comparator;
import java.util.List;

import de.ur.iw.optapeb.ruleseditor.model.Property;
import de.ur.iw.optapeb.ruleseditor.model.PropertyValue;
import de.ur.iw.optapeb.ruleseditor.model.Rule;

/**
 * Defines a kind of lexicographic order, defined by the sequence of properties
 * that were supplied to the constructor of this comparator.
 */
public class LexicographicOrderBasedOnProperties implements Comparator<Rule> {

	private final List<Property> properties;

	public LexicographicOrderBasedOnProperties(List<Property> properties) {
		this.properties = properties;
	}

	@Override
	public int compare(Rule lhs, Rule rhs) {
		for (Property property : properties) {
			if (lhs.conditions.containsKey(property)) {
				if (rhs.conditions.containsKey(property)) {
					// both sides contain property
					PropertyValue lhsFirstValue = lhs.conditions.get(property).iterator().next();
					PropertyValue rhsFirstValue = rhs.conditions.get(property).iterator().next();
					int result = lhsFirstValue.compareTo(rhsFirstValue);
					if (result != 0) {
						return result;
					} // else result == 0 -> continue
				} else {
					// only lhs contains property
					return -1;
				}
			} else {
				if (rhs.conditions.containsKey(property)) {
					// onyl rhs contains property
					return 1;
				} else {
					// neither contains property -> equal -> continue
				}
			}
		}

		// compared all properties - the rules seem to be equal
		return 0;
	}

}