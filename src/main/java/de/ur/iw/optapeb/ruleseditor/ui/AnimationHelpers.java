package de.ur.iw.optapeb.ruleseditor.ui;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.scene.Node;
import javafx.util.Duration;

public class AnimationHelpers {

	static void fadeOut(Node node) {
		fade(node, 0.2);
	}

	static void fadeIn(Node node) {
		fade(node, 1.0);
	}

	private static void fade(Node node, double targetOpacity) {
		FadeTransition fadeTransition = new FadeTransition(Duration.millis(250), node);
		fadeTransition.setInterpolator(Interpolator.EASE_OUT);
		fadeTransition.setToValue(targetOpacity);
		fadeTransition.play();
	}

	static void setFadedOut(Node node) {
		node.setOpacity(0.2);
	}
}
