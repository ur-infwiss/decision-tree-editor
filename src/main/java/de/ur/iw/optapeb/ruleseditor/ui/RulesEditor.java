package de.ur.iw.optapeb.ruleseditor.ui;

import static com.google.common.base.Preconditions.checkNotNull;
import static de.ur.iw.optapeb.ruleseditor.ui.AnimationHelpers.fadeIn;
import static de.ur.iw.optapeb.ruleseditor.ui.AnimationHelpers.fadeOut;
import static de.ur.iw.optapeb.ruleseditor.ui.AnimationHelpers.setFadedOut;
import static java.util.Arrays.asList;
import static javafx.scene.control.Alert.AlertType.ERROR;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.tbee.javafx.scene.layout.MigPane;

import de.ur.iw.optapeb.ruleseditor.FileHelper;
import de.ur.iw.optapeb.ruleseditor.model.DecisionTreeNode;
import de.ur.iw.optapeb.ruleseditor.model.Intervention;
import de.ur.iw.optapeb.ruleseditor.model.Model;
import de.ur.iw.optapeb.ruleseditor.model.ModelAndRules;
import de.ur.iw.optapeb.ruleseditor.model.Property;
import de.ur.iw.optapeb.ruleseditor.model.PropertyValue;
import de.ur.iw.optapeb.ruleseditor.model.Rule;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Duration;
import jfxtras.labs.dialogs.MonologFX;
import jfxtras.labs.dialogs.MonologFXBuilder;
import jfxtras.labs.dialogs.MonologFXButton;
import jfxtras.labs.dialogs.MonologFXButtonBuilder;
import net.miginfocom.layout.CC;
import net.miginfocom.layout.LC;
import net.miginfocom.layout.LayoutUtil;

public class RulesEditor {

	private static final String PREFERENCES_KEY_WINDOW_X = "rulesEditorWindowX";
	private static final String PREFERENCES_KEY_WINDOW_Y = "rulesEditorWindowY";
	private static final String PREFERENCES_KEY_WINDOW_MAXIMIZED = "rulesEditorWindowIsMaximized";
	public static final String PREFERENCES_KEY_LAST_RULES_FILE = "lastRulesFile";

	private static final String RULESET_FILE_EXTENSION = ".json";

	private static final String LABEL_MERGE_BRANCHES = "\u2935\n\u2934"; // \u21ba \u2b43
	private static final String LABEL_SPLIT_BRANCHES = "\u2934\n\u2935"; // \u2195 \u2912 \u2913 \u2b7f \u2945 \u2b86
	private static final String LABEL_SWITCH_RULESET = "\u27a4";

	public static final FileChooser.ExtensionFilter[] EXTENSION_FILTER_FOR_RULES = {
			new ExtensionFilter("Interventionsregeln", "*" + RULESET_FILE_EXTENSION),
			new ExtensionFilter("Alle Dateien", "*.*") };

	private Model model;
	private DecisionTreeNode decisionTreeRoot;
	private DecisionTreeNode lastSavedDecisionTreeRoot;

	private File pathToRules;
	private List<File> otherRuleSetsInSameDirectory = new ArrayList<>();

	private Stage stage;
	private ScrollPane parentForRulesWidgets;

	private Decider ruleTester = null;
	private Map<Property, PropertyValue> lastKnownPatientState = new HashMap<Property, PropertyValue>();

	public RulesEditor(ModelAndRules modelAndRules) throws IOException {
		this.model = modelAndRules.model;
		this.pathToRules = modelAndRules.pathToRules;

		// convert original rules to decision tree, forget rules
		decisionTreeRoot = DecisionTreeNode.createDecisionTreeFromRules(model, modelAndRules.rules);
		lastSavedDecisionTreeRoot = decisionTreeRoot.clone();

		stage = createWindow();
		onPathToRulesChanged();

		updateRulesWidgetsFromDecisionTree();

		stage.show();
	}

	public void setRuleTester(Decider ruleTester) {
		this.ruleTester = ruleTester;
	}

	public void updateTreeBranchesTriggeredByPatientState(Map<Property, PropertyValue> patientState) {
		lastKnownPatientState = patientState;

		// recreate widgets from modified tree
		updateRulesWidgetsFromDecisionTree();
	}

	private void updateRulesWidgetsFromDecisionTree() {
		decisionTreeRoot.computeMarkTriggeredByPatientState(lastKnownPatientState);
		Node rulesWidgets = createRulesWidgetsFromDecisionTree(model, decisionTreeRoot);
		parentForRulesWidgets.setContent(rulesWidgets);
		stage.sizeToScene();
	}

	private void sendRulesToRuleTester() {
		if (ruleTester != null) {
			List<Rule> rules = decisionTreeRoot.createRulesFromDecisionTree(model);
			ruleTester.setNewRules(model, rules);
		}
	}

	private Stage createWindow() throws IOException {
		Stage stage = new Stage();

		stage.getIcons().add(new Image(FileHelper.getResourceAsStream("optapeb.png")));

		MenuBar menuBar = createMenuBar();
		parentForRulesWidgets = new ScrollPane();
		BorderPane borderPane = new BorderPane(parentForRulesWidgets);
		borderPane.setTop(menuBar);
		stage.setScene(new Scene(borderPane));

		stage.setX(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_X, stage.getX()));
		stage.setY(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_Y, stage.getY()));
		stage.setMaximized(MainClass.preferences().getBoolean(PREFERENCES_KEY_WINDOW_MAXIMIZED, stage.isMaximized()));
		stage.xProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_X, newVal.doubleValue());
			}
		});
		stage.yProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_Y, newVal.doubleValue());
			}
		});
		stage.maximizedProperty().addListener((obs, oldVal, newVal) -> {
			MainClass.preferences().putBoolean(PREFERENCES_KEY_WINDOW_MAXIMIZED, newVal.booleanValue());
		});

		return stage;
	}

	private void onPathToRulesChanged() {
		updateKnownRulesets();
		updateStageTitleUsingPathToRules();

		// remember last opened ruleset
		MainClass.preferences().put(PREFERENCES_KEY_LAST_RULES_FILE, pathToRules.getPath());
	}

	private void updateKnownRulesets() {
		if (pathToRules.getParentFile() != null) {
			File[] ruleSets = pathToRules.getParentFile().listFiles((File pathname) -> {
				try {
					if (pathname.isFile() && pathname.canRead()
							&& pathname.getName().endsWith(RULESET_FILE_EXTENSION)) {
						ModelAndRules modelAndRules = FileHelper.loadModelAndRules(pathname);
						// the model of the other rules must match the model of this rules
						return modelAndRules.model.equals(RulesEditor.this.model);
					} else {
						return false;
					}
				} catch (IOException e) {
					// couldn't load rules - ignore them. might have been the model file anyway.
				}
				return false;
			});
			otherRuleSetsInSameDirectory.clear();
			otherRuleSetsInSameDirectory.addAll(asList(ruleSets));

			// skip currently loaded set of rules
			otherRuleSetsInSameDirectory.removeIf(pathToRules::equals);
		} else {
			otherRuleSetsInSameDirectory.clear();
		}
	}

	private void updateStageTitleUsingPathToRules() {
		if (pathToRules != null && pathToRules.getParentFile() != null) {
			Path workingDirectory = FileHelper.getCurrentWorkingDirectory();
			Path pathForTitle = pathToRules.toPath().toAbsolutePath().startsWith(workingDirectory)
					? workingDirectory.relativize(pathToRules.toPath())
					: pathToRules.toPath();
			stage.setTitle("Regeleditor - " + pathForTitle);
		} else {
			stage.setTitle("Regeleditor");
		}
	}

	private MenuBar createMenuBar() {
		MenuItem menuFileNew = new MenuItem("Neu");
		menuFileNew.setOnAction((event) -> {
			if (!lastSavedDecisionTreeRoot.equals(decisionTreeRoot)) {
				MonologFX dialog = new MonologFX(MonologFX.Type.QUESTION);
	            dialog.setTitleText("Warnung - nicht gespeicherte Änderungen");
	            dialog.setMessage("Es gibt nicht gespeicherte Änderungen, die nun verloren gehen würden. Fortsetzen?");
	            if (dialog.showDialog() == MonologFXButton.Type.YES) {
	                //Project p = v_projectList.getSelectionModel().getSelectedItem();
	                //descriptionValue.setText("");
	                //projectModel.projectsProperty().remove(v_projectList.getSelectionModel().getSelectedItem());
	            }
			}

			Path folder;
			if (pathToRules.exists() && pathToRules.getParentFile().exists()) {
				folder = Paths.get(pathToRules.toURI()).getParent();
			} else {
				folder = FileHelper.getCurrentWorkingDirectory();
			}
			pathToRules = folder.resolve(FileHelper.UNNAMED_FILENAME_FOR_RULES).toFile();
			onPathToRulesChanged();

			decisionTreeRoot = DecisionTreeNode.createDecisionTreeFromRules(model, new ArrayList<Rule>());
			updateRulesWidgetsFromDecisionTree();
		});
		MenuItem menuFileOpen = new MenuItem("Laden...");
		menuFileOpen.setOnAction((event) -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Laden...");
			if (pathToRules != null && pathToRules.getParentFile() != null) {
				fileChooser.setInitialDirectory(pathToRules.getParentFile());
			}
			fileChooser.getExtensionFilters().addAll(RulesEditor.EXTENSION_FILTER_FOR_RULES);
			File selectedFile = fileChooser.showOpenDialog(null);
			if (selectedFile != null) {
				loadRules(selectedFile);
			}
		});
		MenuItem menuFileSave = new MenuItem("Speichern");
		menuFileSave.setOnAction((event) -> {
			saveRulesAs(pathToRules);
		});
		MenuItem menuFileSaveAs = new MenuItem("Speichern als...");
		menuFileSaveAs.setOnAction((event) -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Regelsatz speichern");
			if (pathToRules != null && pathToRules.getParentFile() != null) {
				fileChooser.setInitialDirectory(pathToRules.getParentFile());
				fileChooser.setInitialFileName(pathToRules.getName());
			}
			fileChooser.getExtensionFilters().addAll(RulesEditor.EXTENSION_FILTER_FOR_RULES);
			File selectedFile = fileChooser.showSaveDialog(null);
			if (selectedFile != null) {
				saveRulesAs(selectedFile);
			}
		});

		Menu menuFile = new Menu("Interventionsregeln");
		menuFile.getItems().add(menuFileNew);
		menuFile.getItems().add(menuFileOpen);
		menuFile.getItems().add(menuFileSave);
		menuFile.getItems().add(menuFileSaveAs);

		MenuItem menuHelpIntroduction = new MenuItem("Einleitung");
		menuHelpIntroduction.setOnAction((event) -> {
			RichTextPopup.showHTMLResourcePopup("Einleitung", "introduction.html");
		});
		MenuItem menuHelpModelAndRules = new MenuItem("Modell und Regeln");
		menuHelpModelAndRules.setOnAction((event) -> {
			RichTextPopup.showHTMLResourcePopup("Modell und Regeln", "modelandrules.html");
		});
		MenuItem menuHelpUserInterface = new MenuItem("Bedienung");
		menuHelpUserInterface.setOnAction((event) -> {
			RichTextPopup.showHTMLResourcePopup("Bedienung", "userinterface.html");
		});

		Menu menuHelp = new Menu("Hilfe");
		menuHelp.getItems().add(menuHelpIntroduction);
		menuHelp.getItems().add(menuHelpModelAndRules);
		menuHelp.getItems().add(menuHelpUserInterface);

		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().add(menuFile);
		menuBar.getMenus().add(menuHelp);
		return menuBar;
	}

	private void loadRules(File rulesFile) {
		try {
			ModelAndRules modelAndRules = FileHelper.loadModelAndRules(rulesFile);
			if (!modelAndRules.model.equals(model)) {
				new Alert(AlertType.INFORMATION,
						"Sie haben gerade Interventionsregeln geladen, die auf einem anderen Patientenmodell als dem gerade noch geladenen besieren.")
								.show();
			}
			model = modelAndRules.model;
			pathToRules = modelAndRules.pathToRules;
			onPathToRulesChanged();
			decisionTreeRoot = DecisionTreeNode.createDecisionTreeFromRules(model, modelAndRules.rules);
			lastKnownPatientState.clear();
			updateRulesWidgetsFromDecisionTree();
			sendRulesToRuleTester();
		} catch (IOException e) {
			new Alert(AlertType.ERROR, "Fehler beim Laden von '" + rulesFile + "':\n" + e.getLocalizedMessage()).show();
			e.printStackTrace();
		}
	}

	private void saveRulesAs(File file) {
		List<Rule> rules = decisionTreeRoot.createRulesFromDecisionTree(model);
		rules.forEach(Rule::minimizePropertyValues);
		ModelAndRules modelAndRules = new ModelAndRules(file, model, rules);
		try {
			FileHelper.saveModelAndRules(modelAndRules);
			pathToRules = file.toPath().normalize().toFile();
			onPathToRulesChanged();
			updateRulesWidgetsFromDecisionTree();
		} catch (IOException e) {
			new Alert(ERROR, "Fehler beim Speichern von '" + file + "':\n\n" + e.getLocalizedMessage()).show();
		}
	}

	private Node createRulesWidgetsFromDecisionTree(Model model, DecisionTreeNode decisionTreeRoot) {
		MigPane rulesLayoutContainer = new MigPane(new LC().fill());
		createRulesWidgetsFromDecisionTreeRecursive(rulesLayoutContainer, model, decisionTreeRoot);
		return rulesLayoutContainer;
	}

	private void createRulesWidgetsFromDecisionTreeRecursive(MigPane rulesLayoutContainer, Model model,
			DecisionTreeNode decisionTreeNode) {
		if (decisionTreeNode.isRoot()) {
			// create header: list all properties
			for (int i = 0; i < model.properties.size(); i++) {
				Property property = model.properties.get(i);
				Label propertyLabel = new Label(property.getLabel());
				rulesLayoutContainer.add(propertyLabel, new CC().cell(i, 0));
			}
			rulesLayoutContainer.add(new Label("Mikrointervention"));
			rulesLayoutContainer.add(new Label("Wechsle zu Regelsatz..."), new CC().wrap());
			rulesLayoutContainer.getColumnConstraints().fill();

		} else {
			Node propertyValuesToDisplay = createExpandablePropertyValuesWidget(decisionTreeNode);
			int x = decisionTreeNode.depth - 1;
			int y = decisionTreeNode.minLeafIndex * 2 + 1;
			int numCoveredRows = (decisionTreeNode.maxLeafIndex - decisionTreeNode.minLeafIndex) * 2 + 1;
			rulesLayoutContainer.add(propertyValuesToDisplay, new CC().cell(x, y).spanY(numCoveredRows).grow());

			if (decisionTreeNode.isLeaf()) {
				ComboBox<Intervention> interventionComboBox = createInterventionWidget(decisionTreeNode,
						model.interventions);
				interventionComboBox.setOnAction((event) -> {
					decisionTreeNode.intervention = interventionComboBox.getValue();
					updateRulesWidgetsFromDecisionTree();
					sendRulesToRuleTester();
				});
				ComboBox<File> newRulesComboBox = createSwitchRulesWidget(decisionTreeNode,
						otherRuleSetsInSameDirectory, pathToRules.getParentFile());
				newRulesComboBox.setOnAction((event) -> {
					decisionTreeNode.newRules = newRulesComboBox.getConverter().toString(newRulesComboBox.getValue());
					updateRulesWidgetsFromDecisionTree();
					sendRulesToRuleTester();
				});
				Button switchRulesButton = createSwitchRulesButton(decisionTreeNode, newRulesComboBox);
				rulesLayoutContainer.add(interventionComboBox, new CC().cell(x + 1, y));
				rulesLayoutContainer.add(newRulesComboBox, new CC().cell(x + 2, y));
				rulesLayoutContainer.add(switchRulesButton, new CC().cell(x + 3, y).wrap());
			}
		}

		DecisionTreeNode previousChild = null;
		for (DecisionTreeNode child : decisionTreeNode.children) {

			if (child != decisionTreeNode.children.first()) {
				checkNotNull(previousChild);

				// add separation line between this child and the next
				int x = child.depth - 1;
				int y = child.minLeafIndex * 2;

				MenuButton merger = new MenuButton(LABEL_MERGE_BRANCHES);
				merger.setOnMouseEntered((event) -> {
					fadeIn((Node) event.getSource());
				});
				merger.setOnMouseExited((event) -> {
					fadeOut((Node) event.getSource());
				});
				setFadedOut(merger);
				Tooltip tooltipOfMerger = new Tooltip(
						createTooltipTextForMerger(previousChild.propertyValues, child.propertyValues));
				hackTooltipTiming(tooltipOfMerger);
				merger.setTooltip(tooltipOfMerger);
				rulesLayoutContainer.add(merger, new CC().cell(x, y).grow());

				{
					final DecisionTreeNode previousChildFinal = previousChild;
					MenuItem menuItem = new MenuItem("Teilbäume zusammenführen und dabei '"
							+ joinPropertyValueLabelsWithCommasAndOr(previousChildFinal.propertyValues) + "' behalten");
					menuItem.setOnAction(event -> {
						// modify the decision tree (- child and previousChild are now invalid)
						previousChildFinal.absorbSibling(model, child);

						// recreate widgets from modified tree
						updateRulesWidgetsFromDecisionTree();
						sendRulesToRuleTester();
					});
					merger.getItems().add(menuItem);
				}
				{
					final DecisionTreeNode previousChildFinal = previousChild;
					MenuItem menuItem = new MenuItem("Teilbäume zusammenführen und dabei '"
							+ joinPropertyValueLabelsWithCommasAndOr(child.propertyValues) + "' behalten");
					menuItem.setOnAction(event -> {
						// modify the decision tree (- child and previousChild are now invalid)
						child.absorbSibling(model, previousChildFinal);

						// recreate widgets from modified tree
						updateRulesWidgetsFromDecisionTree();
						sendRulesToRuleTester();
					});
					merger.getItems().add(menuItem);
				}

				MigPane filler = new MigPane(new LC().height("2px").insets("0 0 0 0"));
				filler.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
				rulesLayoutContainer.add(filler, new CC().cell(x + 1, y).spanX(LayoutUtil.INF).growX());
			}

			createRulesWidgetsFromDecisionTreeRecursive(rulesLayoutContainer, model, child);

			previousChild = child;
		}
	}

	private Node createExpandablePropertyValuesWidget(DecisionTreeNode decisionTreeNode) {

		Node propertyValuesBox = createPropertyValuesWidget(decisionTreeNode);
		Button expander = new Button();

		Runnable expand = () -> {
			propertyValuesBox.setVisible(true);
			expander.setText("-");

			// ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(100),
			// propertyValuesBox);
			// scaleTransition.setToY(1.0);
			// scaleTransition.setInterpolator(Interpolator.EASE_OUT);
			// scaleTransition.play();
		};
		Runnable collapse = () -> {
			propertyValuesBox.setVisible(false);
			expander.setText("+");

			// ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(100),
			// propertyValuesBox);
			// scaleTransition.setToY(0.0);
			// scaleTransition.play();
		};
		expander.setOnAction(event -> {
			if (propertyValuesBox.isVisible()) {
				collapse.run();
			} else {
				expand.run();
			}
			stage.sizeToScene();
		});

		SortedSet<PropertyValue> propertyValuesToDisplay = decisionTreeNode.propertyValues;
		Property property = propertyValuesToDisplay.first().property;
		if (propertyValuesToDisplay.equals(property.getValues())) {
			collapse.run();
		} else {
			expand.run();
		}
		expander.setVisible(propertyValuesToDisplay.size() > 1);
		expander.setOnMouseEntered((event) -> {
			fadeIn((Node) event.getSource());
		});
		expander.setOnMouseExited((event) -> {
			fadeOut((Node) event.getSource());
		});
		setFadedOut(expander);

		MigPane containerForExpanderAndProperties = new MigPane(new LC().gridGap("0", "0").insets("0 0 0 0").fill());
		containerForExpanderAndProperties.add(expander, new CC().dockWest());
		containerForExpanderAndProperties.add(propertyValuesBox, new CC().hideMode(1).grow());
		return containerForExpanderAndProperties;
	}

	private Node createPropertyValuesWidget(DecisionTreeNode decisionTreeNode) {
		MigPane container = new MigPane(new LC().gridGap("0", "0").insets("0 0 0 0").fill());
		if (!decisionTreeNode.triggeredByPatientState.isEmpty()) {
			container.setBorder(new Border(
					new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		} else {
			container.setBorder(new Border(new BorderStroke(Color.LIGHTGRAY, BorderStrokeStyle.DOTTED,
					CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		}

		SortedSet<PropertyValue> propertyValuesToDisplay = decisionTreeNode.propertyValues;
		Iterator<PropertyValue> propertyValueIterator = propertyValuesToDisplay.iterator();
		while (propertyValueIterator.hasNext()) {
			PropertyValue propertyValue = propertyValueIterator.next();
			Label valueLabel = new Label(propertyValue.value);
			valueLabel.setAlignment(Pos.CENTER);

			CC cc = new CC().grow().pushY();
			if (propertyValueIterator.hasNext()) {
				cc = cc.wrap();
			}
			container.add(valueLabel, cc);

			if (decisionTreeNode.triggeredByPatientState.contains(propertyValue)) {
				addHighlightDueToPatientStateMatch(valueLabel);
			} else {
				hideDueToPatientStateMismatch(valueLabel);
			}

			if (propertyValueIterator.hasNext()) {

				MenuButton splitter = new MenuButton(LABEL_SPLIT_BRANCHES);
				splitter.setOnMouseEntered((event) -> {
					fadeIn((Node) event.getSource());
				});
				splitter.setOnMouseExited((event) -> {
					fadeOut((Node) event.getSource());
				});
				setFadedOut(splitter);
				container.add(splitter, new CC().growX().wrap());
				Tooltip tooltip = new Tooltip(createTooltipTextForSplitter(propertyValue, propertyValuesToDisplay));
				hackTooltipTiming(tooltip);
				splitter.setTooltip(tooltip);

				for (DecisionTreeNode.SplitMode splitMode : DecisionTreeNode.SplitMode.values()) {
					MenuItem menuItem = new MenuItem(splitMode.contextMenuLabel(propertyValue));
					menuItem.setOnAction(event -> {
						// modify the decision tree (- decisionTreeNode is now invalid)
						decisionTreeNode.splitAfter(model, propertyValue, splitMode);

						// recreate widgets from modified tree
						updateRulesWidgetsFromDecisionTree();
						sendRulesToRuleTester();
					});
					splitter.getItems().add(menuItem);
				}
			}
		}

		return container;
	}

	private static String createTooltipTextForMerger(SortedSet<PropertyValue> propertyValuesUpper,
			SortedSet<PropertyValue> propertyValuesLower) {
		return "An dieser Stelle nicht mehr unterscheiden zwischen " + propertyValuesUpper.first().property.getLabel()
				+ ": '" + joinPropertyValueLabelsWithCommasAndOr(propertyValuesUpper) + "' und '"
				+ joinPropertyValueLabelsWithCommasAndOr(propertyValuesLower)
				+ "', sondern beide Fälle gleich behandeln.";
	}

	private static String createTooltipTextForSplitter(PropertyValue propertyValueAfterWhichToSplit,
			SortedSet<PropertyValue> propertyValues) {
		// split propertyValues into two distinct sets
		SortedSet<PropertyValue> propertyValuesOne = new TreeSet<>(propertyValues);
		SortedSet<PropertyValue> propertyValuesTwo = new TreeSet<>(propertyValues);
		propertyValuesOne.removeIf(value -> value.compareTo(propertyValueAfterWhichToSplit) > 0);
		propertyValuesTwo.removeIf(value -> value.compareTo(propertyValueAfterWhichToSplit) <= 0);

		return "An dieser Stelle eine feinere Unterteilung nach " + propertyValues.first().property.getLabel()
				+ " ermöglichen:\n" + "Bisher wird hier nicht unterschieden zwischen den Ausprägungen "
				+ joinPropertyValueLabelsWithCommasAndOr(propertyValues) + ".\n" + "Ermöglicht es, zwischen dem Fall '"
				+ joinPropertyValueLabelsWithCommasAndOr(propertyValuesOne) + "' und dem Fall '"
				+ joinPropertyValueLabelsWithCommasAndOr(propertyValuesTwo) + "' zu unterscheiden.";
	}

	private static String createTooltipTextForSwitchRulesetButton(String newRulesLabel) {
		return "Wechsle zum Regelsatz\n" + newRulesLabel;
	}

	private static String joinPropertyValueLabelsWithCommasAndOr(SortedSet<PropertyValue> propertyValues) {
		StringBuilder result = new StringBuilder();
		int i = 1;
		for (PropertyValue propertyValue : propertyValues) {
			if (i > 1) {
				if (i < propertyValues.size()) {
					result.append(", ");
				} else {
					result.append(" oder ");
				}
			}
			result.append(propertyValue.value);
			i++;
		}

		return result.toString();
	}

	private static void hackTooltipTiming(Tooltip tooltip) {
		try {
			Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
			fieldBehavior.setAccessible(true);
			Object objBehavior = fieldBehavior.get(tooltip);

			Field fieldActivationTimer = objBehavior.getClass().getDeclaredField("activationTimer");
			fieldActivationTimer.setAccessible(true);
			Timeline activationTimer = (Timeline) fieldActivationTimer.get(objBehavior);
			activationTimer.getKeyFrames().clear();
			activationTimer.getKeyFrames().add(new KeyFrame(new Duration(100)));

			Field fieldHideTimer = objBehavior.getClass().getDeclaredField("hideTimer");
			fieldHideTimer.setAccessible(true);
			Timeline hideTimer = (Timeline) fieldHideTimer.get(objBehavior);
			hideTimer.getKeyFrames().clear();
			hideTimer.getKeyFrames().add(new KeyFrame(new Duration(1000 * 60)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static ComboBox<Intervention> createInterventionWidget(DecisionTreeNode decisionTreeNode,
			List<Intervention> items) {
		final ComboBox<Intervention> comboBox = new ComboBox<>(FXCollections.observableArrayList(items));
		comboBox.setValue(decisionTreeNode.intervention);
		if (!decisionTreeNode.triggeredByPatientState.isEmpty()) {
			addHighlightDueToPatientStateMatch(comboBox);
		} else {
			hideDueToPatientStateMismatch(comboBox);
		}
		return comboBox;
	}

	private static ComboBox<File> createSwitchRulesWidget(DecisionTreeNode decisionTreeNode,
			List<File> availableRuleSets, File parentFolder) {
		RulesetFileToLabelConverter rulesFileToLabelConverter = new RulesetFileToLabelConverter(parentFolder);

		final ComboBox<File> comboBox = new ComboBox<>();
		comboBox.setConverter(rulesFileToLabelConverter);
		comboBox.getItems().add(RulesetFileToLabelConverter.NOOP_ENTRY);
		comboBox.getItems().addAll(availableRuleSets);
		comboBox.setValue(rulesFileToLabelConverter.fromString(decisionTreeNode.newRules));

		if (!decisionTreeNode.triggeredByPatientState.isEmpty()) {
			addHighlightDueToPatientStateMatch(comboBox);
		} else {
			hideDueToPatientStateMismatch(comboBox);
		}
		return comboBox;
	}

	private Button createSwitchRulesButton(DecisionTreeNode decisionTreeNode, ComboBox<File> comboBox) {

		Button button = new Button(LABEL_SWITCH_RULESET);
		String newRulesLabel = comboBox.getConverter().toString(comboBox.getValue());
		Tooltip tooltip = new Tooltip(createTooltipTextForSwitchRulesetButton(newRulesLabel));
		hackTooltipTiming(tooltip);
		button.setTooltip(tooltip);
		button.setVisible(!RulesetFileToLabelConverter.NOOP_ENTRY.equals(comboBox.getValue()));
		button.setOnAction((event) -> {
			File file = comboBox.getValue();
			if (!RulesetFileToLabelConverter.NOOP_ENTRY.equals(file)) {
				loadRules(file);
			}
		});

		if (!decisionTreeNode.triggeredByPatientState.isEmpty()) {
			addHighlightDueToPatientStateMatch(button);
		} else {
			hideDueToPatientStateMismatch(button);
		}
		return button;
	}

	private static void addHighlightDueToPatientStateMatch(Region region) {
		// region.setBackground(new Background(new BackgroundFill(Color.DARKSEAGREEN,
		// null, null)));
	}

	private static void hideDueToPatientStateMismatch(Region region) {
		region.setDisable(true);
	}
}
