package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkArgument;
import static de.ur.iw.optapeb.ruleseditor.model.ConsistencyHelpers.checkNoDuplicateElements;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.common.collect.ImmutableSortedSet;

/**
 * Instances of this class are immutable.
 */
public class Property {
	private final String label;
	/**
	 * All possible values of this patient property. The order of this container's
	 * elements matches their semantic order (for example: 1. "low", 2. "medium", 3.
	 * "high"). (and that order is defined by the list given to the constructor)
	 */
	private final SortedSet<PropertyValue> values;

	public Property(String label, List<String> values) {
		// fail fast
		checkArgument(!label.isEmpty());
		checkArgument(!values.isEmpty());
		checkNoDuplicateElements(values);

		this.label = label;

		TreeSet<PropertyValue> propertyValueObjects = new TreeSet<>();
		for (int i = 0; i < values.size(); i++) {
			propertyValueObjects.add(new PropertyValue(this, values.get(i), i));
		}

		this.values = ImmutableSortedSet.copyOf(propertyValueObjects);
	}

	public String getLabel() {
		return label;
	}

	public SortedSet<PropertyValue> getValues() {
		return values;
	}

	public PropertyValue getValueByIndex(int index) {
		for (PropertyValue value : values) {
			if (value.index == index) {
				return value;
			}
		}
		throw new IllegalArgumentException(this + ": has no value for index " + index);
	}

	@Override
	public int hashCode() {
		return label.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null) {
			return false;
		}
		if (getClass() != o.getClass()) {
			return false;
		}
		Property other = (Property) o;
		return label.equals(other.label);
	}

	@Override
	public String toString() {
		return reflectionToString(this, JSON_STYLE);
	}

	/**
	 * For this property, searches the value ranges of all rules for overlaps. The
	 * rules are then split up, so that, for any pair of rules, their value ranges
	 * (only for this property!) are either identical or disjunctive.
	 */
	public void splitRulesToAchieveNonOverlappingPropertyValues(Collection<Rule> rules) {

		// Determine all positions (i.e. index within the property's entire value range)
		// at which said value range needs to be split.
		SortedSet<Integer> splittingIndices = new TreeSet<>();
		for (Rule rule : rules) {
			// add the begin and end of the value range of each rule to the set of positions
			// where splitting needs to occur
			int minIndex = getValues().size();
			int maxIndex = 0;
			for (PropertyValue triggeringValue : rule.getAllTriggeringValuesFor(this)) {
				int index = triggeringValue.index;
				minIndex = Math.min(minIndex, index);
				maxIndex = Math.max(maxIndex, index);
			}
			splittingIndices.add(minIndex);
			splittingIndices.add(maxIndex + 1);
		}

		// splitting at the begin and end of the value range is never meaningful
		splittingIndices.remove(0);
		splittingIndices.remove(getValues().size());

		// do the actual splitting
		for (int splitIndex : splittingIndices) {
			List<Rule> splitOffRules = new ArrayList<>();
			for (Rule rule : rules) {
				Rule splitOffRule = rule.splitBefore(getValueByIndex(splitIndex));
				if (splitOffRule != null) {
					splitOffRules.add(splitOffRule);
				}
			}
			rules.addAll(splitOffRules);
		}
	}
}
