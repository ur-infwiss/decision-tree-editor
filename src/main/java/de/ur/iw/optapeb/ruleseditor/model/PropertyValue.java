package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkNotNull;

public class PropertyValue implements Comparable<PropertyValue> {
	public final Property property;
	public final String value;

	/**
	 * 0-based index that is used to sort all values of a property.
	 */
	public final int index;

	public PropertyValue(Property property, String value, int index) {
		checkNotNull(property);
		checkNotNull(value);
		this.property = property;
		this.value = value;
		this.index = index;
	}

	@Override
	public int compareTo(PropertyValue o) {
		if (property != o.property) {
			throw new IllegalArgumentException("Cannot compare values of different properties: " + this + " and " + o);
		}
		return index - o.index;
	}

	public PropertyValue next() {
		return property.getValueByIndex(index + 1);
	}

	@Override
	public String toString() {
		return property.getLabel() + ": " + value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((property == null) ? 0 : property.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PropertyValue other = (PropertyValue) obj;
		return property.equals(other.property) && value.equals(other.value);
	}

}
