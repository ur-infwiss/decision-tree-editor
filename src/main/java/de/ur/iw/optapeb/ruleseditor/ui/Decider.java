package de.ur.iw.optapeb.ruleseditor.ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ur.iw.optapeb.ruleseditor.FileHelper;
import de.ur.iw.optapeb.ruleseditor.model.Model;
import de.ur.iw.optapeb.ruleseditor.model.Property;
import de.ur.iw.optapeb.ruleseditor.model.PropertyValue;
import de.ur.iw.optapeb.ruleseditor.model.Rule;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Decider {

	private static final String PREFERENCES_KEY_WINDOW_X = "deciderWindowX";
	private static final String PREFERENCES_KEY_WINDOW_Y = "deciderWindowY";
	private static final String PREFERENCES_KEY_WINDOW_MAXIMIZED = "deciderWindowIsMaximized";

	private Model model;
	private List<Rule> rules;
	private Map<Property, PropertyValue> patientState = new HashMap<>();

	private Stage stage;
	private RulesEditor rulesEditor = null;

	public Decider(Model model, List<Rule> rules) throws IOException {
		rules.sort(new LexicographicOrderBasedOnProperties(model.properties));
		this.model = model;
		this.rules = rules;

		stage = createWindow();

		recreateStageContentFromModelAndRules();

		stage.show();
	}

	public void setRulesEditor(RulesEditor rulesEditor) {
		this.rulesEditor = rulesEditor;
	}

	public void setNewRules(Model model, List<Rule> rules) {
		rules.sort(new LexicographicOrderBasedOnProperties(model.properties));
		this.model = model;
		this.rules = rules;
		//this.patientState.clear();

		recreateStageContentFromModelAndRules();
	}

	private void recreateStageContentFromModelAndRules() {
		Node ui = createScene(model, rules);
		((ScrollPane) stage.getScene().getRoot()).setContent(ui);
	}

	private Stage createWindow() throws IOException {
		Stage stage = new Stage();
		stage.getIcons().add(new Image(FileHelper.getResourceAsStream("optapeb.png")));
		stage.setTitle("Entscheidungen veranschaulichen");
		stage.setScene(new Scene(new ScrollPane()));

		stage.setX(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_X, stage.getX()));
		stage.setY(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_Y, stage.getY()));
		stage.setMaximized(MainClass.preferences().getBoolean(PREFERENCES_KEY_WINDOW_MAXIMIZED, stage.isMaximized()));
		stage.xProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_X, newVal.doubleValue());
			}
		});
		stage.yProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_Y, newVal.doubleValue());
			}
		});
		stage.maximizedProperty().addListener((obs, oldVal, newVal) -> {
			MainClass.preferences().putBoolean(PREFERENCES_KEY_WINDOW_MAXIMIZED, newVal.booleanValue());
		});

		return stage;
	}

	private Node createScene(Model model, List<Rule> rules) {

		GridPane container = new GridPane(); // main pane
		container.setHgap(5);
		container.setVgap(20);
		container.setPadding(new Insets(10, 10, 10, 10));

		// list all properties
		for (int i = 0; i < model.properties.size(); i++) {
			// for a better overview, a VBox for every property
			VBox propertyPane = new VBox();
			propertyPane.setPadding(new Insets(10));
			propertyPane.setSpacing(15);
			propertyPane.setBorder(new Border(
					new BorderStroke(Color.BLACK, BorderStrokeStyle.DOTTED, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

			Property property = model.properties.get(i);
			Label propertyLabel = new Label(model.properties.get(i).getLabel());
			propertyPane.getChildren().add(propertyLabel);
			ToggleGroup group = new ToggleGroup();
			// ToggleGroup of all values per property
			// with RadioButtons
			// for an unknown state of property
			RadioButton unknownButton = new RadioButton("unbekannt");
			unknownButton.setToggleGroup(group);
			unknownButton.setSelected(!patientState.containsKey(property));
			unknownButton.setOnAction((event) -> {
				patientState.remove(property);
				onPatientStateChanged();
			});
			propertyPane.getChildren().add(unknownButton);
			// for the regular values
			for (int j = 0; j < model.properties.get(i).getValues().size(); j++) {
				PropertyValue propertyValue = property.getValueByIndex(j);
				RadioButton valueButton = new RadioButton(propertyValue.value);
				valueButton.setToggleGroup(group);
				valueButton.setOnAction((event) -> {
					patientState.put(property, propertyValue);
					onPatientStateChanged();
				});
				valueButton.setSelected(patientState.get(property) == propertyValue);
				propertyPane.getChildren().add(valueButton);
			}
			// add all property groups side by side
			container.add(propertyPane, i, 0);
		} // end for (list all property groups)

		Button makeDecisionButton = new Button("Regel(n) explizit auflisten");
		container.add(makeDecisionButton, 0, 2, model.properties.size(), 1);

		// table for triggered rules
		GridPane outputTable = new GridPane();
		outputTable.setHgap(5);
		outputTable.setVgap(20);
		outputTable.setPadding(new Insets(10, 10, 10, 10));

		container.add(outputTable, 0, 4, model.properties.size() + 5, 1);
		outputTable.setVisible(false);

		makeDecisionButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				int counter = 1; // counts triggered rules (lines in table)
				// lösche letzte Ausgabe
				outputTable.getChildren().clear();

				// table header (properties)
				for (int i = 0; i < model.properties.size(); i++) {
					VBox propertyCell = new VBox();
					propertyCell.setPadding(new Insets(10));
					propertyCell.setSpacing(15);
					propertyCell.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DOTTED,
							CornerRadii.EMPTY, BorderWidths.DEFAULT)));
					propertyCell.getChildren().add(new Label(model.properties.get(i).getLabel()));
					outputTable.add(propertyCell, i + 1, 0);
				}
				// table header (intervention)
				VBox interventionsCell = new VBox();
				interventionsCell.setPadding(new Insets(10));
				interventionsCell.setSpacing(15);
				interventionsCell.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DOTTED,
						CornerRadii.EMPTY, BorderWidths.DEFAULT)));
				interventionsCell.getChildren().add(new Label("Interventionen"));
				outputTable.add(interventionsCell, model.properties.size() + 2, 0);

				// check rules whether triggered
				// list all triggered rules in table
				for (int i = 0; i < rules.size(); i++) {
					Rule rule = rules.get(i);
					if (rule.matches(patientState)) {
						// cell for rule number
						VBox ruleCell = new VBox();
						ruleCell.setPadding(new Insets(10));
						ruleCell.setSpacing(15);
						ruleCell.getChildren().add(new Label("Regel " + counter));
						outputTable.add(ruleCell, 0, counter);

						// trage für alle Konstrukte die möglichen Ausprägungen der aktuell passenden
						// Regel ein
						for (int j = 0; j < model.properties.size(); j++) {
							Property property = model.properties.get(j);
							if (rule.conditions.containsKey(property)) {
								// trage den PropertyValue in die j´te Spalte und counter´te Zeile
								VBox outputCell = new VBox();
								outputCell.setPadding(new Insets(10));
								outputCell.setSpacing(15);
								outputCell.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DOTTED,
										CornerRadii.EMPTY, BorderWidths.DEFAULT)));
								for (PropertyValue value : rule.conditions.get(property)) {
									Label valueLabel = new Label(value.value);
									outputCell.getChildren().add(valueLabel);
								}
								outputTable.add(outputCell, j + 1, counter);
							} else {
								// trage "nicht spezifiziert" in die j´te Spalte und counter´te Zeile
								// scheint nicht notwendig zu sein, bleibt aber mal der Vollständigkeit halber
								// drin
								outputTable.add(new Label("nicht spezifiziert"), j + 1, counter);
							}
						}
						// trage die Intervention der aktuellen Regel ein
						VBox interventionCell = new VBox();
						interventionCell.setPadding(new Insets(10));
						interventionCell.setSpacing(15);
						interventionCell.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DOTTED,
								CornerRadii.EMPTY, BorderWidths.DEFAULT)));
						interventionCell.getChildren().add(new Label(rule.intervention.getLabel()));
						outputTable.add(interventionCell, model.properties.size() + 2, counter);
						counter++;
					}
				}
				outputTable.setVisible(true);
				stage.sizeToScene();

				if (rulesEditor != null) {
					rulesEditor.updateTreeBranchesTriggeredByPatientState(patientState);
				}
			}
		});

		return container;
	}
	
	private void onPatientStateChanged() {
		if (rulesEditor != null) {
			rulesEditor.updateTreeBranchesTriggeredByPatientState(patientState);
		}
	}

}
