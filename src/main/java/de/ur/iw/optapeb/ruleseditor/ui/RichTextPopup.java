package de.ur.iw.optapeb.ruleseditor.ui;

import static com.google.common.base.Charsets.UTF_8;

import java.io.IOException;
import java.util.concurrent.Callable;

import com.google.common.io.Resources;

import de.ur.iw.optapeb.ruleseditor.FileHelper;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class RichTextPopup {

	private Stage stage;
	private ScrollPane parentForWidgets;

	public RichTextPopup(String title, String htmlContent, Callable<Void> callbackOnHidden) throws IOException {
		stage = createWindow(htmlContent);

		if (title != null) {
			stage.setTitle(title);
		} else {
			stage.setTitle("Hilfe");
		}
		
		if (callbackOnHidden != null) {
			stage.setOnHidden((event) -> {
				try {
					callbackOnHidden.call();
				} catch (Exception e) {
					Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
				}
			});
		}

		stage.show();
	}

	private Stage createWindow(String htmlContent) throws IOException {
		Stage stage = new Stage();

		stage.getIcons().add(new Image(FileHelper.getResourceAsStream("optapeb.png")));

		parentForWidgets = new ScrollPane();
		stage.setScene(new Scene(parentForWidgets));

		WebView webView = new WebView();
		parentForWidgets.setContent(webView);
		webView.getEngine().loadContent(htmlContent);

		return stage;
	}

	public static void showHTMLResourcePopup(String title, String helpResourceFilename) {
		try {
			String html = Resources.asCharSource(Resources.getResource("help/" + helpResourceFilename), UTF_8).read();
			new RichTextPopup(title, html, null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
