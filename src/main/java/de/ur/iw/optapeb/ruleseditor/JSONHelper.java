package de.ur.iw.optapeb.ruleseditor;

import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdValueInstantiator;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import de.ur.iw.optapeb.ruleseditor.model.Intervention;
import de.ur.iw.optapeb.ruleseditor.model.Model;
import de.ur.iw.optapeb.ruleseditor.model.Property;
import de.ur.iw.optapeb.ruleseditor.model.PropertyValue;
import de.ur.iw.optapeb.ruleseditor.model.Rule;

public class JSONHelper {
	private static final ObjectMapper objectMapper = new ObjectMapper();

	static {
		objectMapper.registerModule(new ParameterNamesModule(PROPERTIES));
		objectMapper.registerModule(new GuavaModule());
		objectMapper.registerModule(new SimpleModule() {
			private static final long serialVersionUID = -1614653777505036129L;
			{
				addKeySerializer(Property.class, new JsonSerializer<Property>() {
					@Override
					public void serialize(Property value, JsonGenerator gen, SerializerProvider serializers)
							throws IOException {
						gen.writeFieldName(value.getLabel());
					}
				});
				addSerializer(PropertyValue.class, new JsonSerializer<PropertyValue>() {
					@Override
					public void serialize(PropertyValue value, JsonGenerator gen, SerializerProvider serializers)
							throws IOException {
						gen.writeString(value.toString()); // was: value.value
					}
				});
			}
		});
		objectMapper.addMixIn(Rule.class, RuleMixInForSerialization.class);
		objectMapper.addMixIn(Intervention.class, InterventionMixIn.class);
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	}

	public static <T> T deserialize(String json, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		T loaded = objectMapper.readValue(json, clazz);
		return loaded;
	}

	public static List<Rule> deserializeRules(String rulesAsJSON, final Model model)
			throws IOException, JsonParseException, JsonMappingException {

		ObjectMapper objectMapperWithKnownModel = objectMapper.copy();
		objectMapperWithKnownModel.registerModule(new SimpleModule() {
			private static final long serialVersionUID = 8226472866684448870L;
			{
				final String LAST_PROPERTY = "LastProp";

				addKeyDeserializer(Property.class, new KeyDeserializer() {
					@Override
					public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException {
						Property recognizedProperty = model.properties.stream().filter(p -> p.getLabel().equals(key))
								.findFirst().get();
						ctxt.setAttribute(LAST_PROPERTY, recognizedProperty);
						return recognizedProperty;
					}
				});
				addValueInstantiator(PropertyValue.class,
						new StdValueInstantiator(objectMapperWithKnownModel.getDeserializationConfig(),
								TypeFactory.defaultInstance().constructType(PropertyValue.class)) {
							private static final long serialVersionUID = -4676139418410014255L;

							@Override
							public boolean canCreateFromString() {
								return true;
							};

							@Override
							public Object createFromString(DeserializationContext ctxt, String value)
									throws IOException {
								Property lastRecognizedProperty = (Property) ctxt.getAttribute(LAST_PROPERTY);
								for (PropertyValue knownPropertyValue : lastRecognizedProperty.getValues()) {
									if (knownPropertyValue.value.equals(value) ||
											knownPropertyValue.toString().equals(value)) {
										return knownPropertyValue;
									}
								}
								throw new IOException("Could not recognize property value '" + value
										+ "' (last recognized property: " + lastRecognizedProperty + ")");
							};
						});
			}
		});

		objectMapperWithKnownModel.setInjectableValues(new InjectableValues.Std().addValue(Model.class, model));
		objectMapperWithKnownModel.addMixIn(Rule.class, RuleMixInForDeserialization.class);

		List<Rule> rules = objectMapperWithKnownModel.readValue(rulesAsJSON, new TypeReference<List<Rule>>() {
		});
		return rules;
	}

	public static String serialize(Object object) throws JsonProcessingException {
		return objectMapper.writeValueAsString(object);
	}

	private static class RuleMixInForDeserialization {
		@JacksonInject
		public final Model model = null;
	}

	@JsonInclude(Include.NON_NULL)
	private static class RuleMixInForSerialization {
		@JsonIgnore
		public final Model model = null;
	}

	private static class InterventionMixIn {
		@JsonValue
		public final String label = null;
	}
}
