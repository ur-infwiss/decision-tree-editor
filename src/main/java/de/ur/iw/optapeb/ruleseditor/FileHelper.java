package de.ur.iw.optapeb.ruleseditor;

import static com.google.common.base.Charsets.UTF_8;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.io.Resources;

import de.ur.iw.optapeb.ruleseditor.model.Model;
import de.ur.iw.optapeb.ruleseditor.model.ModelAndRules;
import de.ur.iw.optapeb.ruleseditor.model.Rule;

public class FileHelper {

	public static final String FILENAME_FOR_MODEL = "Patientenmodell.json";
	public static final String DEFAULT_FILENAME_FOR_RULES = "Interventionsregeln.json";
	public static final String UNNAMED_FILENAME_FOR_RULES = "unbenannte Regeln.json";

	public static ModelAndRules loadModelAndRules(File pathToRules) throws IOException {
		File pathToModel = pathToRules.toPath().normalize().getParent().resolve(FileHelper.FILENAME_FOR_MODEL).toFile();

		String modelAsJSON = Files.asCharSource(pathToModel, UTF_8).read();
		String rulesAsJSON = Files.asCharSource(pathToRules, UTF_8).read();

		return deserializeModelAndRules(pathToRules, modelAsJSON, rulesAsJSON);
	}

	public static ModelAndRules loadEmbeddedModelAndNoRules() throws IOException {
		String modelAsJSON = Resources.asCharSource(Resources.getResource(FILENAME_FOR_MODEL), UTF_8).read();
		String rulesAsJSON = "[]";

		File pathToRules = FileHelper.getCurrentWorkingDirectory().resolve(UNNAMED_FILENAME_FOR_RULES).toFile();
		return deserializeModelAndRules(pathToRules, modelAsJSON, rulesAsJSON);
	}

	private static ModelAndRules deserializeModelAndRules(File pathToRules, String modelAsJSON, String rulesAsJSON)
			throws IOException, JsonParseException, JsonMappingException {
		Model model = JSONHelper.deserialize(modelAsJSON, Model.class);
		List<Rule> rules = JSONHelper.deserializeRules(rulesAsJSON, model);

		rules.forEach(Rule::fillOmittedPropertyValues);

		return new ModelAndRules(pathToRules, model, rules);
	}

	public static Path getCurrentWorkingDirectory() {
		return Paths.get("").toAbsolutePath();
	}

	public static void saveModelAndRules(ModelAndRules modelAndRules) throws IOException {
		Path parent = modelAndRules.pathToRules.toPath().normalize().getParent();
		File pathToModel = parent.resolve(FileHelper.FILENAME_FOR_MODEL).toFile();
		if (pathToModel.exists()) {
			String oldModelAsJSON = Files.asCharSource(pathToModel, UTF_8).read();
			Model oldModel = JSONHelper.deserialize(oldModelAsJSON, Model.class);
			if (!oldModel.equals(modelAndRules.model)) {
				throw new IOException("Kann die aktuellen Interventionsregeln nicht im Ordner '" + parent.toString()
						+ "' speichern, weil in diesem Ordner bereits ein Patientenmodell vorhanden ist, welches aber nicht zu den zu speichernden Interventionsregeln passt.");
			} else {
				// new model equal to old model - no need to overwrite
			}
		} else {
			// no model in target folder - save current model there
			String modelString = JSONHelper.serialize(modelAndRules.model);
			Files.asCharSink(pathToModel, UTF_8).write(modelString);
		}

		// save/overwrite the rules
		String rulesString = JSONHelper.serialize(modelAndRules.rules);
		Files.asCharSink(modelAndRules.pathToRules, Charsets.UTF_8).write(rulesString);
	}

	public static InputStream getResourceAsStream(String name) throws IOException {
		return Resources.asByteSource(Resources.getResource("optapeb.png")).openStream();
	}
}
