package de.ur.iw.optapeb.ruleseditor.ui;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;

import javafx.util.StringConverter;

public class RulesetFileToLabelConverter extends StringConverter<File> {

	private static final String RULESET_FILE_EXTENSION = ".json";

	public static File NOOP_ENTRY = new File("");

	private final File parentFolder;

	public RulesetFileToLabelConverter(File parentFolder) {
		checkNotNull(parentFolder);
		this.parentFolder = parentFolder;
	}

	@Override
	public String toString(File file) {
		checkNotNull(file);
		if (file == NOOP_ENTRY) {
			return null;
		} else {
			return file.getName().replace(RULESET_FILE_EXTENSION, "");
		}
	}

	@Override
	public File fromString(String filename) {
		if (filename == null || filename.isEmpty()) {
			return NOOP_ENTRY;
		} else if (filename.endsWith(RULESET_FILE_EXTENSION)) {
			return parentFolder.toPath().resolve(filename).toFile();
		} else {
			return parentFolder.toPath().resolve(filename + RULESET_FILE_EXTENSION).toFile();
		}
	}

}
