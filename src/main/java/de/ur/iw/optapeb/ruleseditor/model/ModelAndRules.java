package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.List;

import javax.annotation.Nonnull;

public class ModelAndRules {
	public ModelAndRules(File pathToRules, Model model, List<Rule> rules) {
		checkNotNull(pathToRules);
		checkNotNull(model);
		checkNotNull(rules);
		this.pathToRules = pathToRules;
		this.model = model;
		this.rules = rules;
	}

	@Nonnull
	public final File pathToRules;
	@Nonnull
	public final Model model;
	@Nonnull
	public final List<Rule> rules;

}
