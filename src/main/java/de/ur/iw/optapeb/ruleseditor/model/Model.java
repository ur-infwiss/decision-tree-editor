package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkArgument;
import static de.ur.iw.optapeb.ruleseditor.model.ConsistencyHelpers.checkNoDuplicateElements;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Model {
	/**
	 * All known patient properties, including ones not used in any rules.
	 */
	public final List<Property> properties;

	/**
	 * All possible micro-interventions, including ones not used in any rules.
	 */
	public final List<Intervention> interventions;

	public Model(List<Property> properties, List<Intervention> interventions) {
		this.properties = properties;
		this.interventions = interventions;

		checkArgument(!properties.isEmpty());
		checkArgument(!interventions.isEmpty());
		checkNoDuplicateElements(properties);
		checkNoDuplicateElements(interventions);
	}

	public boolean equals(Model o) {
		return properties.equals(o.properties) && interventions.equals(o.interventions);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

	public Intervention defaultIntervention() {
		return interventions.get(0);
	}
}
