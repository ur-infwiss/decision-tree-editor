package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.SortedSetMultimap;

public class Rule implements Cloneable {
	/**
	 * A reference to the model on top of which this rule is defined.
	 */
	public final Model model;

	/**
	 * This map's keys define all properties that need to be checked when testing
	 * whether this rule matches. For each of those properties, all acceptable
	 * values are then stored here.
	 * <p>
	 * If a known property is not contained in the keySet of this member, then that
	 * property should be treated as always matching when evaluating this rule.
	 * 
	 * @see #matches(Map)
	 */
	public final SortedSetMultimap<Property, PropertyValue> conditions;

	/**
	 * What should be done to the patient if this rule matches?
	 */
	public Intervention intervention;

	/**
	 * After executing this rule's intervention (if any), change the current set of
	 * rules to the ones from this file.
	 */
	public String newRules;

	public Rule(Model model, Multimap<Property, PropertyValue> conditions, Intervention intervention, String newRules) {
		this.model = model;
		this.conditions = MultimapBuilder.hashKeys().treeSetValues().build();
		this.conditions.putAll(conditions);
		this.intervention = intervention;
		this.newRules = newRules;
	}

	/**
	 * Normalize the internal representation of the conditions of this rule: for
	 * each known property whose values are completely omitted from this rule's
	 * conditions, insert the full set of possible values. This does not change the
	 * logic of this rule. Only normalized rules should be used internally.
	 */
	public void fillOmittedPropertyValues() {
		for (Property property : model.properties) {
			if (!conditions.containsKey(property)) {
				conditions.putAll(property, property.getValues());
			}
		}
	}

	/**
	 * Minimize the internal representation of the conditions of this rule: for each
	 * known property whose values are completely filled in this rule's conditions
	 * (i.e. they always match), omit that property entirely. This does not change
	 * the logic of this rule. Only use this representation of rules for storing
	 * them on disk, not for internal processing.
	 */
	public void minimizePropertyValues() {
		for (Property property : model.properties) {
			if (conditions.containsKey(property) && conditions.get(property).equals(property.getValues())) {
				conditions.removeAll(property);
			}
		}
	}

	/**
	 * @return whether this rule matches the given <code>patientState</code>.
	 * @see #conditions
	 */
	public boolean matches(Map<Property, PropertyValue> patientState) {
		for (Property requiredProperty : conditions.keySet()) {
			Collection<PropertyValue> acceptedValues = conditions.get(requiredProperty);
			if (patientState.containsKey(requiredProperty)) {
				PropertyValue currentPatientValue = patientState.get(requiredProperty);
				if (!acceptedValues.contains(currentPatientValue)) {
					return false;
				}
			}
		}

		// Note: If a Property is known in the model, but is not mentioned in the
		// conditions then it is considered matching.

		return true;
	}

	@Override
	public Rule clone() {
		return new Rule(model, conditions, intervention, newRules);
	}

	public boolean willTriggerFor(Property property, PropertyValue value) {
		return (!conditions.containsKey(property) || conditions.get(property).contains(value));
	}

	public SortedSet<PropertyValue> getAllTriggeringValuesFor(Property property) {
		if (!conditions.containsKey(property)) {
			return Collections.unmodifiableSortedSet(property.getValues());
		} else {
			return Collections.unmodifiableSortedSet(conditions.get(property));
		}
	}

	/**
	 * Splits this instance into two distinct rules that complement each other and
	 * together cover the same condition space as this instance did before this
	 * method call. This instance becomes the first of the two rules. The return
	 * value of this function is the second rule.
	 * <p>
	 * The split occurs based on a given property value: the condition space will be
	 * split before the supplied property value, so that the value itself belongs to
	 * the second rule.
	 * 
	 * @param propertyValue
	 *            determines where the condition space will be split. This value
	 *            will belong to the second rule.
	 * @return the new, second rule. If one of the two new sets of property values
	 *         is empty after the split (i.e. if there was no real split), then the
	 *         return value is <code>null</code> and this rule instance keeps all
	 *         its property values.
	 */
	public Rule splitBefore(PropertyValue propertyValue) {
		Rule two = clone();

		Set<PropertyValue> valuesOne = conditions.get(propertyValue.property);
		Set<PropertyValue> valuesTwo = two.conditions.get(propertyValue.property);
		valuesOne.removeIf(value -> value.index >= propertyValue.index);
		valuesTwo.removeIf(value -> value.index < propertyValue.index);

		if (valuesOne.isEmpty()) {
			valuesOne.addAll(valuesTwo);
			return null;
		} else if (valuesTwo.isEmpty()) {
			return null;
		} else {
			return two;
		}
	}

	/**
	 * Splits this into two distinct rules that complement each other. This instance
	 * becomes the first of the two rules. The return value of this function is the
	 * second rule.
	 * <p>
	 * The split occurs based on the values of one property: the value range for
	 * which this rule triggers will be split among the two new rules.
	 * 
	 * @param property
	 *            determines which properties' value range will be split.
	 * @param splitIndex
	 *            is the index of the first value that no longer triggers the first
	 *            rule, but the second.
	 * @return the new, second rule. If one of the two new sets of property values
	 *         is empty after the split (i.e. if there was no real split), then the
	 *         return value is <code>null</code> and this rules keeps all its
	 *         property values.
	 */
	@Deprecated
	public Rule splitOnProperty(Property property, int splitIndex) {
		checkArgument(splitIndex > 0 && splitIndex < property.getValues().size());

		Rule two = clone();

		Set<PropertyValue> valuesOne = conditions.get(property);
		Set<PropertyValue> valuesTwo = two.conditions.get(property);
		valuesOne.removeIf(value -> value.index >= splitIndex);
		valuesTwo.removeIf(value -> value.index < splitIndex);

		if (valuesOne.isEmpty()) {
			valuesOne.addAll(valuesTwo);
			return null;
		} else if (valuesTwo.isEmpty()) {
			return null;
		} else {
			return two;
		}
	}
}
