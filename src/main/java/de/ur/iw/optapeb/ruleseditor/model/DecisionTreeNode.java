package de.ur.iw.optapeb.ruleseditor.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.NotImplementedException;

import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SortedSetMultimap;

public class DecisionTreeNode implements Comparable<DecisionTreeNode> {
	/*
	 * the actual tree structure:
	 */

	public final DecisionTreeNode parent;
	public final SortedSet<DecisionTreeNode> children = new TreeSet<>();

	/*
	 * OPTAPEB semantics (i.e. parts of the rules):
	 */

	// the propertyValues of a node must not be changed after instantiation, because
	// its parent uses them to sort its children
	public final ImmutableSortedSet<PropertyValue> propertyValues;

	// only leaf nodes have an intervention
	public Intervention intervention = null;
	public String newRules = null;

	// if some of the values of this node match a given patient
	// state, then this contains them; can be empty, which means everything matches
	public final SortedSet<PropertyValue> triggeredByPatientState = new TreeSet<>();

	/*
	 * properties that are derived from the tree structure:
	 */

	public int depth;
	public int numberOfNodes;
	public int minLeafIndex; // "leaf index": all leafs are numbered (0-based);
	public int maxLeafIndex; // non-leafs store the min and max values of their children

	/*
	 * Helper class for computing the derived tree properties
	 */
	private static class TreeTraversalState {
		public int nextFreeLeafIndex = 0;
	}

	private DecisionTreeNode() {
		this.propertyValues = ImmutableSortedSet.of();
		this.parent = null;
	}

	private DecisionTreeNode(SortedSet<PropertyValue> propertyValues, DecisionTreeNode parent) {
		checkNotNull(propertyValues);
		checkArgument((parent == null) == (propertyValues.isEmpty()));
		this.propertyValues = ImmutableSortedSet.copyOf(propertyValues);
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		return propertyValues.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		} else if (o == null) {
			return false;
		} else if (getClass() != o.getClass()) {
			return false;
		}
		DecisionTreeNode rhs = (DecisionTreeNode) o;

		return propertyValues.equals(rhs.propertyValues) && Objects.equals(intervention, rhs.intervention)
				&& Objects.equals(newRules, rhs.newRules) && children.equals(rhs.children);
	}

	@Override
	public int compareTo(DecisionTreeNode rhs) {
		if (this.equals(rhs)) {
			return 0;
		} else {
			return propertyValues.first().compareTo(rhs.propertyValues.first());
		}
	}

	@Override
	public String toString() {
		if (propertyValues != null) {
			return property().getLabel() + ": "
					+ propertyValues.stream().map((pv) -> pv.value).reduce((s1, s2) -> s1 + "," + s2).get();
		} else {
			return "root node";
		}
	}
	
	public DecisionTreeNode clone() {
		checkState(isRoot());
		return createDeepCopyWithSamePropertyValuesAndGivenParent(null);
	}

	private DecisionTreeNode createDeepCopyWithSamePropertyValuesAndGivenParent(DecisionTreeNode newParent) {
		return createDeepCopyWithGivenPropertyValuesAndGivenParent(propertyValues, newParent);
	}

	private DecisionTreeNode createDeepCopyWithGivenPropertyValuesAndSameParent(
			SortedSet<PropertyValue> newPropertyValues) {
		return createDeepCopyWithGivenPropertyValuesAndGivenParent(newPropertyValues, parent);
	}

	private DecisionTreeNode createDeepCopyWithGivenPropertyValuesAndGivenParent(
			SortedSet<PropertyValue> newPropertyValues, DecisionTreeNode newParent) {
		DecisionTreeNode copyOfThis = new DecisionTreeNode(newPropertyValues, newParent);
		for (DecisionTreeNode child : children) {
			copyOfThis.children.add(child.createDeepCopyWithSamePropertyValuesAndGivenParent(copyOfThis));
		}
		copyOfThis.intervention = intervention;
		copyOfThis.newRules = newRules;

		// TODO: copying these values makes no sense - they need to be recomputed anyway
		copyOfThis.depth = depth;
		copyOfThis.numberOfNodes = numberOfNodes;
		copyOfThis.minLeafIndex = minLeafIndex;
		copyOfThis.maxLeafIndex = maxLeafIndex;

		return copyOfThis;
	}

	public static enum SplitMode {
		MinimalBranchFirst {
			@Override
			public String contextMenuLabel(PropertyValue propertyValueBeforeSplit) {
				return "Neue Regel für '" + propertyValueBeforeSplit + "' abspalten";
			}
		},
		DeepCopy {
			@Override
			public String contextMenuLabel(PropertyValue propertyValueBeforeSplit) {
				return "Bestehende Regeln für '" + propertyValueBeforeSplit + "' und '" + propertyValueBeforeSplit.next() + "' duplizieren";
			}
		},
		MinimalBranchLast {
			@Override
			public String contextMenuLabel(PropertyValue propertyValueBeforeSplit) {
				return "Neue Regel für '" + propertyValueBeforeSplit.next() + "' abspalten";
			}
		};

		public abstract String contextMenuLabel(PropertyValue propertyValueBeforeSplit);
	}

	public void splitAfter(Model model, PropertyValue propertyValueAfterWhichToSplit, SplitMode splitMode) {
		// the root node must not be split
		checkNotNull(parent);

		// make sure that both parts will be non-empty
		checkArgument(propertyValues.contains(propertyValueAfterWhichToSplit));
		checkArgument(!propertyValues.last().equals(propertyValueAfterWhichToSplit));

		// split propertyValues into two distinct sets
		SortedSet<PropertyValue> propertyValuesOne = new TreeSet<>(propertyValues);
		SortedSet<PropertyValue> propertyValuesTwo = new TreeSet<>(propertyValues);
		propertyValuesOne.removeIf(value -> value.compareTo(propertyValueAfterWhichToSplit) > 0);
		propertyValuesTwo.removeIf(value -> value.compareTo(propertyValueAfterWhichToSplit) <= 0);

		// create two new subtrees based on the two sets of PropertyValues
		DecisionTreeNode newNodeOne;
		DecisionTreeNode newNodeTwo;
		switch (splitMode) {
		case DeepCopy:
			newNodeOne = this.createDeepCopyWithGivenPropertyValuesAndSameParent(propertyValuesOne);
			newNodeTwo = this.createDeepCopyWithGivenPropertyValuesAndSameParent(propertyValuesTwo);
			break;
		case MinimalBranchFirst:
			newNodeOne = new DecisionTreeNode(propertyValuesOne, parent);
			newNodeTwo = this.createDeepCopyWithGivenPropertyValuesAndSameParent(propertyValuesTwo);
			break;
		case MinimalBranchLast:
			newNodeOne = this.createDeepCopyWithGivenPropertyValuesAndSameParent(propertyValuesOne);
			newNodeTwo = new DecisionTreeNode(propertyValuesTwo, parent);
			break;
		default:
			throw new NotImplementedException("missing case statement?");
		}

		// finally, modify the actual tree structure:
		// replace this with newNodeOne and newNodeTwo
		parent.children.remove(this);
		parent.children.add(newNodeOne);
		parent.children.add(newNodeTwo);

		// tree structure has changed - update derived properties
		getRoot().fixMissingChildrenAndInterventions(model);
		getRoot().computeDerivedTreeProperties();

		// this node is no longer valid
	}

	public void absorbSibling(Model model, DecisionTreeNode sibling) {
		// the root node must not be manipulated in this way
		checkNotNull(parent);

		// make sure that sibling is located directly before or after this node
		checkArgument(sibling.property().equals(property())
				&& (sibling.propertyValues.last().index + 1 == propertyValues.first().index
						|| propertyValues.last().index + 1 == sibling.propertyValues.first().index));

		// create two new subtrees based on the two sets of PropertyValues

		SortedSet<PropertyValue> mergedPropertyValues = new TreeSet<>(propertyValues);
		mergedPropertyValues.addAll(sibling.propertyValues);
		DecisionTreeNode newThis = this.createDeepCopyWithGivenPropertyValuesAndSameParent(mergedPropertyValues);

		// finally, modify the actual tree structure:
		// replace this with newThis and remove sibling
		parent.children.remove(this);
		parent.children.remove(sibling);
		parent.children.add(newThis);

		// tree structure has changed - update derived properties
		getRoot().fixMissingChildrenAndInterventions(model);
		getRoot().computeDerivedTreeProperties();

		// this node is no longer valid
	}

	public boolean isRoot() {
		return parent == null;
	}

	public DecisionTreeNode getRoot() {
		return isRoot() ? this : parent.getRoot();
	}

	public boolean isLeaf() {
		return children.isEmpty();
	}

	private void computeDerivedTreeProperties() {
		computeDerivedTreePropertiesRecursive(new TreeTraversalState());
	}

	private void computeDerivedTreePropertiesRecursive(TreeTraversalState traversalState) {
		if (isRoot()) {
			depth = 0;
		} else {
			depth = parent.depth + 1;
		}

		numberOfNodes = 1;
		for (DecisionTreeNode child : children) {
			child.computeDerivedTreePropertiesRecursive(traversalState);
			numberOfNodes += child.numberOfNodes;
		}

		if (isLeaf()) {
			minLeafIndex = traversalState.nextFreeLeafIndex;
			maxLeafIndex = minLeafIndex;
			traversalState.nextFreeLeafIndex++;
		} else {
			minLeafIndex = children.first().minLeafIndex;
			maxLeafIndex = children.last().maxLeafIndex;
		}
	}

	public static DecisionTreeNode createDecisionTreeFromRules(Model model, List<Rule> rules) {
		DecisionTreeNode rootNode = new DecisionTreeNode();
		createChildNodesFromRulesRecursive(model.properties, rules, rootNode);
		rootNode.fixMissingChildrenAndInterventions(model);
		rootNode.computeDerivedTreeProperties();
		return rootNode;
	}

	private static void createChildNodesFromRulesRecursive(List<Property> properties, List<Rule> allRulesOfThisNode,
			DecisionTreeNode node) {
		if (properties.isEmpty()) {
			checkArgument(!allRulesOfThisNode.isEmpty());
			node.intervention = allRulesOfThisNode.get(0).intervention;
			node.newRules = allRulesOfThisNode.get(0).newRules;
			return;
		} else {
			// split the list of properties into head and tail
			// (i.e. splittingProperties and remainingProperties)
			Property splittingProperty = properties.get(0);
			List<Property> remainingProperties = properties.subList(1, properties.size());

			// process all rules that we received: split them up based on their conditions,
			// so they can be uniquely mapped to child nodes
			splittingProperty.splitRulesToAchieveNonOverlappingPropertyValues(allRulesOfThisNode);

			// build a map from SortedSet<PropertyValue> to all Rules which trigger on
			// precisely those values:
			ListMultimap<SortedSet<PropertyValue>, Rule> splitRuleSets = MultimapBuilder.hashKeys().arrayListValues()
					.build();
			for (Rule rule : allRulesOfThisNode) {
				SortedSet<PropertyValue> propertyValueGroup = rule.getAllTriggeringValuesFor(splittingProperty);
				splitRuleSets.put(propertyValueGroup, rule);
			}

			// recursive calls to generate all children of this node
			for (Entry<SortedSet<PropertyValue>, List<Rule>> entry : Multimaps.asMap(splitRuleSets).entrySet()) {
				DecisionTreeNode child = new DecisionTreeNode(entry.getKey(), node);
				node.children.add(child);
				createChildNodesFromRulesRecursive(remainingProperties, entry.getValue(), child);
			}
		}
	}

	public List<Rule> createRulesFromDecisionTree(Model model) {
		checkState(isRoot());

		List<Rule> rules = new ArrayList<Rule>();
		Multimap<Property, PropertyValue> conditions = MultimapBuilder.hashKeys().treeSetValues().build();
		for (DecisionTreeNode child : children) {
			child.createRulesFromDecisionTreeRecursive(model, conditions, rules);
		}
		return rules;
	}

	private void createRulesFromDecisionTreeRecursive(Model model, Multimap<Property, PropertyValue> conditions,
			List<Rule> rules) {
		checkState(!isRoot());

		Property property = property();
		conditions.putAll(property, propertyValues);

		if (isLeaf()) {
			Rule newRule = new Rule(model, conditions, intervention, newRules);
			rules.add(newRule);
		} else {
			for (DecisionTreeNode child : children) {
				child.createRulesFromDecisionTreeRecursive(model, conditions, rules);
			}
		}

		conditions.removeAll(property);
	}

	public void fixMissingChildrenAndInterventions(Model model) {
		checkState(isRoot());
		fixMissingChildrenAndInterventionsRecursive(model.properties, model.defaultIntervention());
	}

	private void fixMissingChildrenAndInterventionsRecursive(List<Property> properties,
			Intervention defaultIntervention) {
		if (properties.isEmpty()) { // leaf
			if (intervention == null) {
				intervention = defaultIntervention;
			}
		} else { // non-leaf
			// look at all children: which values are we missing among them?
			SortedSet<PropertyValue> allPropertyValues = properties.get(0).getValues();
			SortedSet<PropertyValue> missingPropertyValues = new TreeSet<>(allPropertyValues);
			for (DecisionTreeNode child : children) {
				missingPropertyValues.removeAll(child.propertyValues);
			}

			// group the missing values: direct "neighbors" (in their index space) go into
			// the same group
			SortedSetMultimap<Integer, PropertyValue> groups = MultimapBuilder.hashKeys().treeSetValues().build();
			int i = 0;
			for (PropertyValue missingPropertyValue : missingPropertyValues) {
				groups.put(missingPropertyValue.index - i, missingPropertyValue);
				i++;
			}

			// create a new tree node from each group
			for (SortedSet<PropertyValue> group : Multimaps.asMap(groups).values()) {
				DecisionTreeNode newNode = new DecisionTreeNode(group, this);
				children.add(newNode);
			}

			// visit entire tree
			List<Property> propertiesForChildren = properties.subList(1, properties.size());
			for (DecisionTreeNode child : children) {
				child.fixMissingChildrenAndInterventionsRecursive(propertiesForChildren, defaultIntervention);
			}
		}
	}

	public void computeMarkTriggeredByPatientState(Map<Property, PropertyValue> patientState) {
		checkState(isRoot());

		for (DecisionTreeNode child : children) {
			child.clearMarkTriggeredByPatientStateRecursive();
			child.computeMarkTriggeredByPatientStateRecursive(patientState);
		}
	}

	private void clearMarkTriggeredByPatientStateRecursive() {
		triggeredByPatientState.clear();
		for (DecisionTreeNode child : children) {
			child.clearMarkTriggeredByPatientStateRecursive();
		}
	}

	private boolean computeMarkTriggeredByPatientStateRecursive(Map<Property, PropertyValue> patientState) {
		checkState(!isRoot());

		// if we're here then all of this node's parents do match

		// test whether this node matches the given patientState
		boolean thisNodeMatchesPatientState = !patientState.containsKey(property())
				|| propertyValues.contains(patientState.get(property()));

		if (!thisNodeMatchesPatientState) {
			return false;
		}

		// all parents do match and this node matches -> continue to check the children
		boolean anyChildMatchesPatientState = false;
		for (DecisionTreeNode child : children) {
			anyChildMatchesPatientState |= child.computeMarkTriggeredByPatientStateRecursive(patientState);
		}

		boolean highlightThisNode = thisNodeMatchesPatientState && (isLeaf() || anyChildMatchesPatientState);
		if (highlightThisNode) {
			// this node is part of a node chain that matches the current patientState

			if (!patientState.containsKey(property())) {
				// patientState does not even mention this note's property
				// -> highlight all property values
				triggeredByPatientState.addAll(property().getValues());
			} else {
				// highlight only the property value from patientState
				triggeredByPatientState.add(patientState.get(property()));
			}
		}

		return highlightThisNode;
	}

	private Property property() {
		return propertyValues.first().property;
	}

	public int distanceToNearestBranchToPreviousLeaf() {
		checkState(isLeaf());
		return 1 + parent.distanceToNearestBranchToPreviousLeafRecursive(this);
	}

	private int distanceToNearestBranchToPreviousLeafRecursive(DecisionTreeNode caller) {
		DecisionTreeNode lastChild = null;
		for (DecisionTreeNode child : children) {
			if (child == caller) {
				if (lastChild != null) {
					return 0;
				} else {
					return 1 + parent.distanceToNearestBranchToPreviousLeafRecursive(this);
				}
			}
			lastChild = child;
		}
		throw new IllegalStateException("caller is not among children -> bug!");
	}
}
