package de.ur.iw.optapeb.ruleseditor.model;

import com.google.common.base.Preconditions;

/**
 * An intervention is a regular text that says what to do with the patient (i.e.
 * "Calm down.").
 */
public class Intervention {
	private final String label;

	public Intervention(String label) {
		Preconditions.checkNotNull(label);
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public String toString() {
		return getLabel();
	}

	@Override
	public int hashCode() {
		return label.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Intervention other = (Intervention) obj;
		if (label == null) {
			if (other.label != null) {
				return false;
			}
		} else if (!label.equals(other.label)) {
			return false;
		}
		return true;
	}

}
